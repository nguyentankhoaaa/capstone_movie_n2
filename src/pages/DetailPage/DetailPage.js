import React, { useEffect, useRef, useState } from "react";
import { NavLink, useParams } from "react-router-dom";
import { movieServ } from "../../service/movieService";
import { Progress, Tabs } from "antd";
import ItemDetail from "./TabsDetail.js/ItemDetail";
import { Button, Modal } from 'antd';
import { theaterServ } from "../../service/theaterService";

export default function DetailPage() {
  const [open, setOpen] = useState(false);
  let { id } = useParams();
  const [movie, setMovie] = useState([]);
  const [movieTheater, setMovieTheater] = useState([]);

  useEffect(() => {
    let fetchDetail = async () => {
      try {
        let result = await movieServ.getInforMovie(id);
        console.log(result);
        
        setMovie(result.data.content);
       
      } catch (error) {
        console.log(error);
      }
    };
    let movieTheater = async () => {
      try {
        let cimena = await movieServ.getMovieTheater(id);
        console.log(cimena);
        setMovieTheater(cimena.data.content.heThongRapChieu);
     
      } catch (error) {
        console.log(error);
      }
    };
 
    movieTheater();
    fetchDetail();
  }, []);

  const onChange = (key) => {
    console.log(key);
  };

  let renderRapChieu = () => {
    return movieTheater.map((cumRapChieu) => {
      return {
        key: cumRapChieu.maHeThongRap,
        label: <img src={cumRapChieu.logo} className="h-12" alt="" />,
        children: (
          <Tabs
            defaultActiveKey="1"
            items={cumRapChieu.cumRapChieu.map((rap) => {
              return {
                key: rap.maRap,
                label: (
                  <div className="text-black">{cumRapChieu.tenHeThongRap} </div>
                ),

                children: (
                  <div className="overflow-y-scroll space-y-10">
                    {rap.lichChieuPhim.map((item) => {
                      return <ItemDetail movie={item} />;
                    })}
                  </div>
                ),
              };
            })}
            onChange={onChange}
          />
        ),
      };
    });
  };


  const videos = document.querySelectorAll('iframe')

  
  window.addEventListener('click', () => {
     videos.forEach(i => {
        const source = i.src
        i.src = ''
        i.src = source
     })
  })


  return (
    <div className="container detailpage">
      <div className=" px-5 py-28 item-detail  flex h-full flex-grow">
        <div className="w-1/4  item-img ">
          <img
            src={movie.hinhAnh}
            className="rounded-xl img"
            style={{ height: 500 }}
            alt=""
          /> 
    
     
   
          <button className=" trailer-btn   px-5 py-2 mt-1 ml-16 bg-blue-600 text-white rounded-xl"  onClick={() => setOpen(true)}>
            Xem Trailler
          </button>
        </div>
        <div className="w-1/2 ml-10  item-content">
          <p className="font-medium text-lg text-orange-600">
            Tên phim:{" "}
            <span className="text-base text-black">{movie.tenPhim}</span>
          </p>
          <p className="font-medium text-lg text-orange-600 mt-6">
            Nội dung phim:{" "}
            <span className="text-base text-black">{movie.moTa}</span>
          </p>
          <p className="font-medium text-lg text-orange-600 mt-6">
            Đánh giá phim:{" "}
            <span style={{ width: "50%" }}>
              {" "}
              <Progress percent={movie.danhGia * 10} />
            </span>
          </p>
          <p className="font-medium text-lg text-orange-600 mt-6">
            Ngày khởi chiếu:{" "}
            <span className="text-base text-green-600">
              {movie.ngayKhoiChieu}
            </span>
          </p>
          <div className="">
        <Tabs
          className="tab  bg-gray-50 py-5 mt-2 rounded-xl"
          style={{ width: 700 }}
          tabPosition="left"
          defaultActiveKey="1"
          items={renderRapChieu()}
          onChange={onChange}
        />
      </div>
        </div>
      </div>
    {/* Modal */}
   
      <Modal 
        centered
        open={open}
        onOk={() => setOpen(false)}
        onCancel={() => setOpen(false)}
        width={1000}
      >

  <iframe  id="videoId" width="97%"  height="450px" src={movie.trailer}  frameborder="0"  ></iframe>

      </Modal>
    </div>
  );
}
