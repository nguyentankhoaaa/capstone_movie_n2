import React from "react";
import { NavLink } from "react-router-dom";

export default function ItemDetail({ movie }) {
  return (
    <div>
     <NavLink to={`/booking/${movie.maLichChieu}`}>
     <p className="text-lg font-medium">{movie.ngayChieuGioChieu}</p>
     </NavLink>
    </div>
  );
}
