import { message } from 'antd';
import React from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { setThongTinDatGhe } from '../../toolkit/BookingSlice';
import { localMovie } from '../../service/localMovie';

export default function ThongTinDatGhe({thongTinPhim,infor}) {
let dispatch = useDispatch();
let userInfor = useSelector ((state)=>{
  return state.userSlice.userInfor
})
  let danhSachGheDangDat=  useSelector(
    state => state.bookingSlice.danhSachGheDangDat
     );
     console.log(danhSachGheDangDat);
   let renderTenGhe = ()=>{
    return danhSachGheDangDat.map((ghe)=>{
      return   <span className='text-right'>Ghế {ghe.tenGhe}, </span>
    })
   }
   let renderGiaTien = ()=>{
     let sum =0;
    for (let index = 0; index < danhSachGheDangDat.length; index++) {
      const item = danhSachGheDangDat[index];
      sum += item.giaVe
    
    }
    return sum
   }
   let handleDatVe =  ()=>{
     if(userInfor == null){
      message.error("Bạn Chưa Đăng Nhập !")
      return false
    }
   else if(renderGiaTien()  == 0){
       message.error("Bạn Chưa Chọn Ghế !")
       return false;
      }
      else{
      message.success("Đặt Vé Thành Công")

    }
   }
 
  return (
    <div className='p-5  bg-slate-100 w-full mt-4 text-black shadow-xl rounded-lg '>
      <div>
       <h2 className='text-center text-3xl text-green-600 font-base pb-2'>{renderGiaTien()} VNĐ</h2>
       <hr style={{backgroundColor:"black"}} className='border-2 shadow-xl w-full' />
       <div className='flex justify-between p-5'>
     <h1 className='text-base'>Cụm Rạp :</h1>
     <p className='text-green-700 font-base text-base'>{thongTinPhim.tenCumRap}</p>
    
       </div>
       <hr style={{backgroundColor:"black"}} className='border-2 shadow-xl w-full' />
       <div className='flex justify-between p-5'>
     <h1 className='text-base w-1/3'>Địa Chỉ :</h1>
     <p className='text-green-700 font-base text-base '>{thongTinPhim.diaChi}
     </p>
    
       </div>
       <hr style={{backgroundColor:"black"}} className='border-2 shadow-xl w-full' />
       <div className='flex justify-between p-5'>
     <h1 className='text-base'> Rạp :</h1>
     <p className='text-green-700 font-base text-base'>{thongTinPhim.tenRap}</p>
    
       </div>
       <hr style={{backgroundColor:"black"}} className='border-2 shadow-xl w-full' />
       <div className='flex justify-between p-5'>
     <h1 className='text-base'>Ngày  Chiếu :</h1>
     <p className='text-green-700 font-base text-base'>{thongTinPhim.ngayChieu}</p>
       </div>
       <hr style={{backgroundColor:"black"}} className='border-2 shadow-xl w-full' />
       <div className='flex justify-between p-5'>
     <h1 className='text-base'>Tên Phim :</h1>
     <p className='text-green-700 font-base text-base'>{thongTinPhim.tenPhim}</p>
    
       </div>
       <hr style={{backgroundColor:"black"}} className='border-2 shadow-xl w-full' />
       <div className='flex justify-content-between p-5'>
     <h1 className='text-base w-1/3'>Chọn :</h1>
     <p className='text-green-700 font-base text-base '>{renderTenGhe()}</p>

    
       </div>
       <hr style={{backgroundColor:"black"}} className='border-2 shadow-xl w-full' />
       <button  onClick={handleDatVe}  className='w-full mt-5 bg-red-500 py-3 text-white font-medium 
       text-xl rounded-lg'>ĐẶT VÉ</button>
      </div>
    </div>
  )
}
