import React, { Fragment, useEffect, useState } from 'react';
import './BookingPage.css';
import img from "../../assest/img/bgmovie.jpg"
import ThongTinDatGhe from './ThongTinDatGhe';
import Header from '../../components/Header/Header';
import HangGhe from './HangGhe';
import { theaterServ } from '../../service/theaterService';
import { useParams } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { setLoadingOff, setLoadingOn } from '../../toolkit/spinnerSlice';


export default function BookingPage() {
let {id}   = useParams();
const [rap, setRap] = useState([])
const [infor, setinfor] = useState([]);
let dispath = useDispatch();
useEffect(() => {
  let listPhongVe  = async () =>{
    dispath(setLoadingOn())
    try {
      let result =await  theaterServ.getListPhongVe(id);
      setRap(result.data.content.danhSachGhe);
      dispath(setLoadingOff())
      setinfor(result.data.content.thongTinPhim);
    } catch (error) {
      console.log(error);
    }
  }
  listPhongVe()
}, [])

  

  return (
   <div className='h-full w-full'  >
    <Header/>
     <div className='z-30 bookingMovie container ' style={{width:"100%",
    height:"100vh",backgroundImage:`url(${img})`,backgroundSize:"100%",backgroundRepeat:"no-repeat"}}>
        <div className='overlay' style={{backgroundColor:"rgba(0,0,0,0.7)",position:"absolute",
        width:"100%",height:"100vh"}}>
<div className='container-fluid px-20 mt-20'>
    <div className='boxDatGhe flex justify-content-center align-items-center'>
   <div className='mt-14 dayGhe '>
<HangGhe rap={rap}  />
<div className='goiYCho  mt-6 font-medium text-white flex justify-content-center ml-80 '>
      <div className='text-center'> 
      <button className='gheDuocChon text-white'></button>
      <br />
     <h2 className=' text-center'> Đã đặt</h2>
      </div>
   
   <div className='ml-5 text-center'>
   <button className='gheVip text-white'></button>
     <br />
     <h2 className=' text-center'>Vip</h2>
   </div>
   
   <div className='ml-5 text-center'>
   <button className='ghe text-white'  style={{marginLeft:0}}></button>
     <br />
     <h2 className=' text-center'>Thường</h2>
   </div>
    </div>
   </div>
    <div className= 'w-1/3 text-gray-300 font-medium text-xl ml-10 thongTinGhe'>

   <ThongTinDatGhe thongTinPhim = {infor} />
     
    </div>

    </div>
    </div> 
        </div>
     </div>

   </div>
  )
}
