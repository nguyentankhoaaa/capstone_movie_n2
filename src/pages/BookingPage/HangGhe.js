import React from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { datGhe } from '../../toolkit/BookingSlice';

export default function HangGhe({rap}) {
  let dispatch = useDispatch();
  let danhSachGheDangDat=  useSelector(
state => state.bookingSlice.danhSachGheDangDat
 );
  let renderGhe =  ()=>{
    return rap.map((item)=>{
      let buttonCss = "";
      let disabled = false;
      let cssGheDangDat = "";
  
      let indexGheDangDat = danhSachGheDangDat.
      findIndex(gheDangDat => gheDangDat.tenGhe == item.tenGhe
      )
      if(indexGheDangDat != -1){
       cssGheDangDat = "gheDangChon"
      }
    if(item.daDat){
     buttonCss="gheDuocChon";
     disabled = true;
     return <button className={`ghe ${buttonCss} font-medium  ${disabled}`}>X</button>
    }



    if(item.loaiGhe === "Thuong"){
      buttonCss="ghe";
      return  <button onClick={()=>{
        dispatch(datGhe(item))
      }} className={`ghe ${buttonCss} font-medium ${cssGheDangDat} ${disabled}`}>{item.tenGhe}</button>
    }else{
      buttonCss="gheVip";
      return  <button  onClick={()=>{
        dispatch(datGhe(item))
      }} className={`ghe ${buttonCss}  font-medium ${cssGheDangDat} ${disabled}`}>{item.tenGhe}</button>
    }
      
    
  })
  }
  return (
    <div>{renderGhe()}</div>
  )
}
