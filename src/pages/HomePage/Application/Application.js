import React from 'react'
import img from "../../../assest/img/NETFLIX-Screen.webp"
export default function Application() {
  return (
    <div className=' application  ' id='ungDung'>
        <div className='content  z-20   '>
<h2 className='text-white text-3xl font-bold '>Ứng dụng tiện lợi dành cho người yêu điện ảnh</h2>
<p className='text-white font-medium mt-16 mb-16'>Không chỉ đặt vé, bạn còn có thể bình luận phim, chấm điểm rạp và đổi quà hấp dẫn.</p>
<a target='blank' href='https://play.google.com/store/apps/details?id=com.netflix.mediaclient&hl=vi&gl=US' className='bg-red-600 text-white px-3 py-4 z-1 font-medium rounded-xl'>APP MIỄN PHÍ - TẢI VỀ NGAY</a>
<h5 className='text-white font-medium mt-10'>NETFLIX có hai phiên bản IOS & Android</h5>

        </div>
<div className='img    z-20 '>
    <img src={img} className='object-contain pt-10 pr-32 pb-10 ' style={{height:"700px",width:"100%"}}  alt="" />
</div>
    </div>
  )
}
