import React from "react";
import MovieList from "./MovieList/MovieList";
import CarouselMovie from "./MovieList/CarouselMovie";
import TabMovie from "./TabMovie/TabMovie";
import Application from "./Application/Application";

export default function HomePage() {
  return (
    <div className="bg-slate-800 body" id="body">
      <CarouselMovie />
      <MovieList />
      <TabMovie />
   
    </div>
  );
}
