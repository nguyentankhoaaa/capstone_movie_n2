import React from 'react'
import { NavLink } from 'react-router-dom'

export default function ItemTabMovie({movie}) {
  return (
    <div className='flex item-tab ' >
        <img src={movie.hinhAnh} className='h-44 w-40 object-cover object-top' alt="" />
   <div>
   <h2 className='ml-2 font-medium text-lg'>{movie.tenPhim}</h2>
        <div  className='grid grid-cols-2 '>
     {movie.lstLichChieuTheoPhim.slice(0,4).map((item)=>{
        return  <div>
           <NavLink to={`/booking/${item.maLichChieu}`}>
           <p className='px-2 py-2 ml-2 bg-gray-100 mt-2 font-medium rounded-xl text-green-700'> 
              {item.ngayChieuGioChieu}
            <br/>  <span className='text-red-600 ffont-medium'>{item.tenRap}</span>
         </p>
           </NavLink>
       
        </div>
       })}
     </div>
   </div>
    </div>
  )
}
