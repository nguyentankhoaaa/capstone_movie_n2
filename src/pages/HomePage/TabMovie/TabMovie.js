import React, { useEffect, useState } from "react";
import { movieServ } from "../../../service/movieService";
import { Tabs } from "antd";
import ItemTabMovie from "./ItemTabMovie";


export default function TabMovie() {
  const [heThongRap, setHeThongRap] = useState([]);

  useEffect(() => {
    movieServ
      .getMovieByTheater()
      .then((res) => {
       console.log(res);
        setHeThongRap(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  const onChange = (key) => {
    console.log(key);
  };

  let renderRap = () => {
    return heThongRap.map((cumRap) => {
      return {
        key: cumRap.maHeThongRap,
        label: <img src={cumRap.logo}  className=" logo h-20" alt="" />,
        children: (
          <Tabs className="children-tab-movie"
            style={{ height: 700 }}
            tabPosition="left"
            defaultActiveKey="1"
            items={cumRap.lstCumRap.map((rap) => {
              return {
                key: rap.tenCumRap,
                label: (
                  <div className="text-left">
                    <p className="text-green-600 font-medium text-sm">
                      {rap.tenCumRap}
                    </p>
                    <p className="text-gray-800">
                      {" "}
                      {rap.diaChi.slice(0, 40) + "..."}
                    </p>
                    <a className="text-red-600 font-medium">[Chi Tiết]</a>
                  </div>
                ),

                children: (
                  <div
                    className="children-tab-movie   overflow-y-scroll space-y-10"
                    style={{ height: 680 }}
                  >
                    {rap.danhSachPhim.slice(0, 9).map((item) => {
                      return <ItemTabMovie movie={item} />;
                    })}
                  </div>
                ),
              };
            })}
            onChange={onChange}
          />
        ),
      };
    });
  };

  return (
    <div className="tab-movie" id="cumRap">
      <h2 className="container pt-20 content py-5 text-orange-600 font-medium text-2xl">
        HỆ THỐNG RẠP
      </h2>
      <div className=" tab container pb-20 flex justify-center items-center">
        <Tabs
          className="   bg-gray-50 py-5 rounded-xl"
          style={{ width: 1100 }}
          tabPosition="left"
          defaultActiveKey="1"
          items={renderRap()}
          onChange={onChange}
        />
      </div>
    </div>
  );
}
