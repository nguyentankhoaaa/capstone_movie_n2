import React, { useEffect, useState } from 'react'

import { movieServ } from '../../../service/movieService';

import ItemMovie from './ItemMovie';
import { Carousel } from 'antd';
const contentStyle = {
  margin: 0,
  height: '160px',
  color: '#fff',
  lineHeight: '160px',
  textAlign: 'center',
  background: '#364d79',
};

export default function MovieList() {
    const [movie, setMovie] = useState([]);

    useEffect(() => {
 
   movieServ.getMovieInfor().then((res) => {
 
           setMovie(res.data.content);
         })
         .catch((err) => {
          console.log(err);
         }); 
    }, []);
 
    
  return (
  <div className='movie-list ' >
    <h2 className='   container pt-20 px-20 py-5 text-orange-600 font-medium text-2xl'>PHIM HOT TRONG TUẦN</h2>
      <div className='item-list grid  container '>
            {
        movie.map((item)=>{
            return    <div className=''>
                 <ItemMovie movie={item}/>
            </div>
        })
        } 
  
    </div>
  
  </div>
  )
}
