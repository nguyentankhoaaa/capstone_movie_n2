import { Carousel } from 'antd';
import { useEffect, useState } from 'react';
import { movieServ } from '../../../service/movieService';
const CarouselMovie = () => {
  const [movie, setMovie] = useState([]);
  useEffect(() => {
movieServ.getBannerMovie()
.then((res) => {
       
        setMovie(res.data.content)
      })
      .catch((err) => {
       console.log(err);
      });
  }, [])
  
  const contentStyle = {
    height: '700px',
    color: '#fff',
    lineHeight: '400px',
    width:"100%",
    backgroundSize:"cover"
   
  };
  return (
    <Carousel autoplay>
      {movie.map((item)=>{
       return <div className='carousel'>
              <img src={item.hinhAnh} style={contentStyle} className='img object-full ' alt="" />
       </div>
      })}
     
    </Carousel>
  );
}
export default CarouselMovie;