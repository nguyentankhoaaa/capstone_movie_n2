import { Card } from 'antd';
import { useDispatch } from 'react-redux';
import { NavLink } from 'react-router-dom';

const { Meta } = Card;
const ItemMovie = ({movie}) => {


  return (
    <Card hoverable className='card mb-10' 
      cover={<img alt="example" src={movie.hinhAnh} className='object-cover  object-top img' style={{height:350}}/>}
    >
      <Meta title={movie.tenPhim} 
      description={<NavLink className="navlink text-blue-500 text-lg font-medium" to={`/detail/${movie.maPhim}`}><a onClick="scroll(o,0)">Xem Chi Tiết</a></NavLink>} />
    </Card>

    
  );
}
export default ItemMovie;