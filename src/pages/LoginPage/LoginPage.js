import React from "react";
import { Button, Form, Input, message } from "antd";
import { useDispatch } from "react-redux";
import { NavLink, useNavigate } from "react-router-dom";
import Lottie from "lottie-react";
import login_animate from "../../assest/login_icon.json";
import { setUserLogin } from "../../toolkit/userSlice";
import { userServ } from "../../service/userService";
import { localServ } from "../../service/localService";
import logo from "../../assest/logo.json";
import { setLoadingOff, setLoadingOn } from "../../toolkit/spinnerSlice";

const LoginPage = () => {
  let dispatch = useDispatch();
  let navigate = useNavigate();


  const onFinishToolKit = (values) => {
    userServ
    .postLogin(values)
    .then((res) => {
      message.success("đăng nhập thành công");
      localServ.set(res.data.content);
      navigate("/");
      dispatch(setUserLogin(res.data.content));
    
    
      })
      .catch((err) => {
        message.error("đăng nhập thất bại");
        console.log(err);
      });
  };

  const onFinishFailed = (errorInfo) => {};
  return (
    <div className="login-page   w-screen h-screen flex justify-center items-center   bg-gray-800 "  >
    
          <div className="container  p-5 bg-slate-100 rounded-xl " style={{background:"#F4EEE0"}}>
         <div className='lottie'>
         <Lottie className="item"  animationData={logo} loop={true} />
         </div>
         <div className='content'>
       <div className=" logo">
      <i className="fa fa-user p-3 bg-red-500 rounded-3xl ml-8 mb-2 " style={{color:"white"}} />
         <h2 className=" text-black text-xl font-medium"> Đăng Nhập</h2>
       </div>
         <Form  className="form "
        
name="basic" 
labelCol={{
  span: 8,
}}
wrapperCol={{
  span: 24,
}}

initialValues={{
  remember: true,
}}
onFinish={onFinishToolKit}
onFinishFailed={onFinishFailed}
autoComplete="off"
layout="vertical"
>
<Form.Item

  label="Tài Khoản"
  name="taiKhoan"
  rules={[
    {
      required: true,
      message: "Please input your username!",
    },
  ]}
>
  <Input />
</Form.Item>
<Form.Item
  label="Mật Khẩu"
  name="matKhau"
  rules={[
    {
      required: true,
      message: "Please input your password!",
    },
  ]}
>
  <Input.Password />
</Form.Item>


  <Form.Item  
    wrapperCol={{
      span: 24,
    }}
    className="text-right  "
  >
    <Button 
      className="bg-red-600 button-login  text-white"
      htmlType="submit"
    >
      Đăng Nhập
    </Button>
  </Form.Item>

   <NavLink to="/signup" className="pt-2 text-right font-medium">
  <p className="text-base title">Bạn chưa có tài khoản ? Đăng Ký</p>
   </NavLink>

</Form>
         </div>
          </div>
      </div>
  );
};

export default LoginPage;

