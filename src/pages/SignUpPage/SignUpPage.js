import { Button, Form,Input, Select, message,} from 'antd';
import Lottie from "lottie-react";
import logo from "../../assest/logo.json";
import { userServ } from '../../service/userService';
import { NavLink, useNavigate } from 'react-router-dom';
const SignUpPage = () => {
  
    let navigate= useNavigate();
      const tailFormItemLayout = {
        wrapperCol: {
          xs: {
            span: 24,
            offset: 0,
          },
          sm: {
            span: 16,
            offset: 8,
        },
    },
};
    const [form] = Form.useForm();
    const onFinish = (values) => {
      console.log('Received values of form: ', values);
      userServ.postSignUp(values)
      .then((res) => {
              console.log(res);
              message.success("Đăng Ký Thành Công!");
              navigate("/")
            })
            .catch((err) => {
             console.log(err);
             message.error("Tài Khoản hoặc Email đã tồn tại");
            });
    };

    return (
      <div className=" signup-page   w-screen  h-screen flex justify-center items-center   bg-gray-800 "  >
          <div className="container   bg-slate-100 rounded-xl " style={{background:"#F4EEE0"}}>
         <div className='lottie'>
         <Lottie className="item"  animationData={logo} loop={true} />
         </div>
         <div className="content">
         <div className="logo">
      <i className="fa fa-lock p-3 bg-red-500 rounded-3xl ml-8 mb-2 " style={{color:"white"}} />
         <h2 className=" text-black text-xl font-medium ml-2 mb-2"> Đăng Ký</h2>
       </div>
         <Form  className='form'
                layout='vertical'
                form={form}
                name="register"
                onFinish={onFinish}
                initialValues={{
                  residence: ['zhejiang', 'hangzhou', 'xihu'],
                  prefix: '96',
                }}
            
                scrollToFirstError
              >
                 <Form.Item
                  name="taiKhoan"
                  label="Tài Khoản"
                  tooltip="What do you want others to call you?"
                  rules={[
                    {
                      required: true, 
                      message: 'Please input your nickname!',
                      whitespace: true,
                    },
                  ]}
                >
                  <Input />
                </Form.Item>
                <Form.Item
                  name="hoTen"
                  label="Tên Người Dùng "
                  rules={[
                    {
                      type: 'string',
                      message: 'The input is not valid E-mail!',
                    },
                    {
                      required: true,
                      message: 'Please input your name!',
                    },
                  ]}
                >
                  <Input />
                </Form.Item>
                <Form.Item
                  name="email"
                  label="E-mail"
                  rules={[
                    {
                      type: 'email',
                      message: 'The input is not valid E-mail!',
                    },
                    {
                      required: true,
                      message: 'Please input your E-mail!',
                    },
                  ]}
                >
                  <Input />
                </Form.Item>
                <Form.Item
                  name="matKhau"
                  label="Mật Khẩu"
                  rules={[
                    {
                      required: true,
                      message: 'Please input your password!',
                    },
                  ]}
                  hasFeedback
                >
                  <Input.Password />
                </Form.Item>
                <Form.Item
                  name="confirm"
                  label="Nhập Lại Mật Khẩu"
                  dependencies={['matKhau']}
                  hasFeedback
                  rules={[
                    {
                      required: true,
                      message: 'Please confirm your password!',
                    },
                    ({ getFieldValue }) => ({
                      validator(_, value) {
                        if (!value || getFieldValue('matKhau') === value) {
                          return Promise.resolve();
                        }
                        return Promise.reject(new Error('The two passwords that you entered do not match!'));
                      },
                    }),
                  ]}
                >
                  <Input.Password />
                </Form.Item>
              
                <Form.Item {...tailFormItemLayout} className='text-right'>
                <Button  htmlType="submit" className=' bg-red-600 text-white button-signup'>
                  Đăng ký
                </Button>
              </Form.Item>
                <NavLink to="/login" className="pt-2 font-medium text-right">
      <p className='text-base'>Bạn đã có tài khoản ? Đăng Nhập</p>
   </NavLink>
              </Form>
         </div>
          </div>
      </div>
    );
  };
  export default SignUpPage;