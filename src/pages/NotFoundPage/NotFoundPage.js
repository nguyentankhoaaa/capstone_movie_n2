import React from 'react'
import { NavLink } from 'react-router-dom'

export default function NotFoundPage() {
  return (
    <div className='pt-44'>
   <section className="page_404 ">
  <div className="container">
    <div className="row">
      <div className="col-sm-12 ">
        <div className="col-sm-10 col-sm-offset-1  text-center">
          <div className="four_zero_four_bg">
            <h1 className="text-center ">404</h1>
          </div>
          <div className="contant_box_404">
        
            <p className='font-medium text-lg'>Trang này không tồn tại!</p>
           <NavLink to="/">
           <a href className="link_404 rounded-xl bg-red-600 font-medium">Go to Home</a>
           </NavLink>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

    </div>
  )
}
