import Layout from "../Layout/Layout";
import BookingPage from "../pages/BookingPage/BookingPage";
import DetailPage from "../pages/DetailPage/DetailPage";
import HomePage from "../pages/HomePage/HomePage";
import LoginPage from "../pages/LoginPage/LoginPage";
import NotFoundPage from "../pages/NotFoundPage/NotFoundPage";
import SignUpPage from "../pages/SignUpPage/SignUpPage";

export const pageRoutes = [
  {
    url: "/",
    component: <Layout component={<HomePage />} />,
  },
  {
    url: "/signup",
    component: <SignUpPage />,
  },
  {
    url: "/login",
    component: <LoginPage />,
  },
  {
    url: "/detail/:id",
    component: <Layout component={<DetailPage />} />,
  },
  {
    url: "*",
    component: <Layout component={<NotFoundPage />} />,
  },
  {
    url: "/booking/:id",
    component: <BookingPage />,
  },
];
