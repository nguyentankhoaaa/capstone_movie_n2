import React, { useEffect, useState } from 'react'
import { movieServ } from '../../service/movieService';
import lion   from '../../assest/img/lion.png';
import tb_lion    from '../../assest/img/daThongBao_lion.png';
import Application from '../../pages/HomePage/Application/Application';
export default function Footer() {
  const [rap, setRap] = useState([]);
  useEffect(() => {
  movieServ.getInforTheater().then((res) => {
          
          setRap(res.data.content);
        })
        .catch((err) => {
         console.log(err);
        });
  }, []);
  let renderTheater = ()=>{
    return rap.map((item)=>{
      return   <img src={item.logo} className='h-10 w-10 ' alt="" />
     
    })
  }
  
  return (
  <div style={{background:"#212121"}} className='pb-16 footer'>
      
      <div className='container footer-content    grid grid-cols-4 px-10 py-10 footer' >
      <div className='ml-10'>
        <h1 className='text-white'>NETFLIX</h1>
        <div className='grid grid-cols-2 mt-8'>
        <nav style={{color:"#9E9E9E"}} >
          <p>FAQ-thỏa thuậ bảo mật</p>
          
          <p>Brand Guidelines</p>
        </nav>
        <nav style={{color:"#9E9E9E"}} >
          <p>Thỏa thuận sử dụng</p>
          <p>Chính sách bảo mật</p>
        </nav>
        </div>
 
      </div>
      <div className='ml-10' >
        <h1 className='text-white'>Đối Tác</h1>
        <div className='grid grid-cols-3 gap-3 logo-theater  mt-8'>
       {renderTheater()}
        </div>
 
      </div>
      <div className='ml-10'>
        <h1 className='text-white'>MOBILE-APP</h1>
        <div className='grid grid-cols-2 mt-8'>
        <nav className='text-3xl '>
  <i className="fab fa-apple " />
  <i className="fab fa-android" />

</nav>

        </div>
 
      </div>
      <div className='ml-10'>
        <h1 className='text-white'>MOBILE-APP</h1>
        <div className='grid grid-cols-2 mt-8'>
        <nav className='text-3xl '>
        <i class="fab fa-facebook"></i>
        <i class="fab fa-twitter"></i>
</nav>

        </div>
 
      </div>
      
   
    </div>
    <div className='container px-10 py-10 '>
    <hr  className='bg-white border-1 text-white mb-7 ' style={{width:"100%"}}/>
  <div className='grid grid-cols-3 footer-logo'>
  <img src={lion} alt="" />
  <div className='text-white text-sm  mr-10 title'>
  <p>NETFLIX – SẢN PHẨM CỦA CÔNG TY CỔ PHẦN ZION</p>
  <p className='mt-5   '>
  Địa chỉ: Z06 Đường số 13, Phường Tân Thuận Đông, Quận 7, Tp. Hồ Chí Minh, Việt Nam.
Giấy chứng nhận đăng ký kinh doanh số: 0101659783,
đăng ký thay đổi lần thứ 30, ngày 22 tháng 01 năm 2020 do Sở kế hoạch và đầu tư Thành phố Hồ Chí Minh cấp.
Số Điện Thoại (Hotline): 1900 545 436
  </p>
  </div>
  <img src={tb_lion} style={{width:200}}  className='ml-10 ' alt="" />
  </div>
    </div>
  </div>
  )
}
