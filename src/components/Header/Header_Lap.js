import React from 'react'
import UserMenu from './userMenu'
import { NavLink } from 'react-router-dom'

export default function Header_Lap() {
  return (
<div className='bg-gray-200 fixed z-40 w-full shadow-xl header'>
<div className='container flex justify-between items-center p-4 shadow-y-xl'>
    <NavLink to="/">
    <a onClick="scroll(0,0)" className='text-4xl font-medium text-red-600'>NETFLIX</a>
    </NavLink>
      <div>
      <nav className="nav flex mx-3">
  <a className="nav-link active" href="#cumRap">Lịch Chiếu</a>
  <a className="nav-link" href="#cumRap">Cụm Rạp</a>
  <a className="nav-link" href="#">Tin Tức</a>
  <a className="nav-link disabled" href="#ungDung">Ứng Dụng</a>
</nav>
      </div>
      <div>
        <UserMenu/>
      </div>

    </div>

    
</div>
  )
}
