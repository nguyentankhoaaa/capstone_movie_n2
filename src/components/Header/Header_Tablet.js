import React from 'react'
import UserMenu from './userMenu'
import { NavLink } from 'react-router-dom'
export default function Header_Tablet() {
  return (
    <div>
        <div className='px-4 header-tablet bg-gray-200 fixed z-50 w-full shadow-xl header'>
<div className=' flex justify-between items-center p-4  shadow-y-xl'>

    <NavLink to="/">
    <a onClick="scoll(0,0)" className='text-4xl font-medium text-red-600'>NETFLIX</a>
    </NavLink>

<label htmlFor='nav-input' className='nav-btn'>
<i className="fa fa-bars rounded-sm" />
</label>
<input className='nav_input' type="checkbox" id='nav-input' />

    <nav className='nav rounded-b-xl'>
       <div className='p-5'>  
       <UserMenu />
       </div>
       <hr  style={{width:"100%"}} />
    <a className="nav-link active" href="#">Lịch Chiếu</a>
  <a className="nav-link" href="#">Cụm Rạp</a>
  <a className="nav-link" href="#">Tin Tức</a>
  <a className="nav-link disabled" href="#">Ứng Dụng</a>
    </nav>
<label className='nav-overlay' htmlFor='nav-input'></label>
    </div>
</div>
    </div>
  )
}
