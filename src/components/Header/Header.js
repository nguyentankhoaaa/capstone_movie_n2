import React from 'react'
import { Desktop, Mobile, Tablet } from '../../Layout/responsive'
import Header_Lap from './Header_Lap'
import Header_Tablet from './Header_Tablet'
import Header_mobile from './Header_mobile'

export default function Header() {
  return (
    <div>
      <Desktop>
        <Header_Lap/>
      </Desktop>
      <Tablet>
        <Header_Tablet/>
      </Tablet>
      <Mobile>
        <Header_mobile/>
      </Mobile>
    </div>
  )
}
