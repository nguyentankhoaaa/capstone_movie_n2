import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { NavLink } from "react-router-dom";
import { localServ } from "../../service/localService";
import { message } from "antd";
import { setLoadingOn } from "../../toolkit/spinnerSlice";

export default function UserMenu() {
  let dispatch = useDispatch()
  let userInfor = useSelector((state) => {
    return state.userSlice.userInfor;
  });
  let handleLogout = () => {
    localServ.remove();
    window.location.reload();
    message.success("Đăng Xuất Thành Công!");
    dispatch(setLoadingOn())
  };
  let renderContent = () => {
    let buttonCss = "px-5 py-2 border-2 border-red-600 rounded-xl text-red-600";
    if (userInfor) {
      return (
        <>
          <h2 className="text-green-600 font-medium cursor-pointer"><i className=" fa fa-user" />  {userInfor.hoTen}</h2>
          <NavLink to="/">
            <button onClick={handleLogout} className={buttonCss}>Đăng xuất</button>
          </NavLink>
        </>
      );
    } else {
      return (
        <>
          <NavLink to="/login">
            <button className={buttonCss} onClick={handleLogin}>Đăng nhập</button>
          </NavLink>
          <NavLink to="/signup">
            <button className={buttonCss} onClick={handleSignUp}>Đăng ký</button>
          </NavLink>
        </>
      );
    }
  };
  let handleLogin = () =>{
    window.location.href="/login";
    dispatch(setLoadingOn())
  }
  let handleSignUp = () =>{
    window.location.href="/signup";
    dispatch(setLoadingOn())
  }
  return <div className="space-x-5 flex items-center"> {renderContent()} </div>;
}
