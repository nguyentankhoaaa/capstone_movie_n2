import React from "react";
import { useSelector } from "react-redux";
import { PuffLoader } from "react-spinners";
export default function Spinner() {
  let { isLoading } = useSelector((state) => {
    return state.spinnerSlice;
  });
  return isLoading ? (
    <div
      style={{ background: "#577D86" }}
      className="h-screen w-screen fixed top-0 left-0 z-50 flex justify-center items-center"
    >
      <PuffLoader color="#D21312" size={80} />
    </div>
  ) : (
    <></>
  );
}
