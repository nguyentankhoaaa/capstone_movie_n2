import logo from './logo.svg';
import './App.css';
import   {BrowserRouter, Route, Routes} from "react-router-dom"
import { pageRoutes } from './Routes/Routes';
import Spinner from './components/Spinner/Spinner';

function App() {
  return (
    <div>
   
  <Spinner/>
  <BrowserRouter>
  <Routes>
    {pageRoutes.map(({url,component})=>{
      return <Route path={url} element={component} />
    })}
  </Routes>
  </BrowserRouter>
    </div>
  );
}

export default App;
