export const localMovie = {
    get: () => {
      let dataJson = localStorage.getItem("MOVIE");
      return JSON.parse(dataJson);
    },
    set: (userInfo) => {
      let dataJson = JSON.stringify(userInfo);
      localStorage.setItem("MOVIE", dataJson);
    },
    remove: () => {
      localStorage.removeItem("MOVIE");
    },
  };
  