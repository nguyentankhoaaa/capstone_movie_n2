import axios from "axios";
import { BASE_URL, configHeader, https } from "./config";

export const userServ = {
  postSignUp: (signUpForm) => {
    return https.post("/api/QuanLyNguoiDung/DangKy", signUpForm);
  },
  postLogin: (loginForm) => {
    return axios({
      url: `${BASE_URL}/api/QuanLyNguoiDung/DangNhap`,
      method: "POST",
      data: loginForm,
      headers: configHeader(),
    });
  },
};
