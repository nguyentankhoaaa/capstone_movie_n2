import { https } from "./config";

export const movieServ = {
  getBannerMovie: () => {
    return https.get("/api/QuanLyPhim/LayDanhSachBanner");
  },
  getMovieInfor: () => {
    return https.get("/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP07");
  },
  getMovieByTheater: () => {
    return https.get(
      "/api/QuanLyRap/LayThongTinLichChieuHeThongRap?maNhom=GP07"
    );
  },
  getInforTheater: () => {
    return https.get("/api/QuanLyRap/LayThongTinHeThongRap");
  },
  getInforMovie: (id) => {
    return https.get(`/api/QuanLyPhim/LayThongTinPhim?MaPhim=${id}`);
  },
  getMovieTheater: (id) => {
    return https.get(`/api/QuanLyRap/LayThongTinLichChieuPhim?MaPhim=${id}`);
  },
};
