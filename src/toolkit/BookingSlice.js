import { createSlice } from "@reduxjs/toolkit"
import { localMovie } from "../service/localMovie";

let initialState = {
    danhSachGheDangDat:[
    ],
  
}
const bookingSlice = createSlice({
name:"bookingSlice",
initialState,
reducers:{
datGhe:(state,action)=>{
    let danhSachGheDangDatUpdate=[...state.danhSachGheDangDat];
    let index = danhSachGheDangDatUpdate.findIndex(
        gheDangDat=>gheDangDat.tenGhe === action.payload.tenGhe);
        if(index !== -1){
       danhSachGheDangDatUpdate.splice(index,1)
        }
        else{
            danhSachGheDangDatUpdate.push(action.payload)
        }
        state.danhSachGheDangDat = danhSachGheDangDatUpdate;
},


},
});
export const {datGhe} = bookingSlice.actions;
export default bookingSlice.reducer;