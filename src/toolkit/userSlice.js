import { createSlice } from "@reduxjs/toolkit";
import { localServ } from "../service/localService";

const initialState = {
  userInfor: localServ.get(),
};

const userSlice = createSlice({
  name: "userSlice",
  initialState,
  reducers: {
    setUserLogin: (state, action) => {
      state.userInfor = action.payload;
    },
  },
});
export const { setUserLogin } = userSlice.actions;

export default userSlice.reducer;
