import React from 'react'
import Header from '../components/Header/Header'
import Footer from '../components/Footer/Footer'
import Application from '../pages/HomePage/Application/Application'

export default function Layout({component}) {
  return (
    <div>
      <Header/>
       {component}
       <Application/>
      <Footer/>
    </div>
  )
}
